import sublime
import sublime_plugin
import xml.dom.minidom


class XmlPrettyPrintCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        # Get the entire region
        view_region = sublime.Region(0, self.view.size())
        # Get the region text
        region_text = self.view.substr(view_region)
        # Instantiate the DOM object with our XML string
        try:
        	xml_dom = xml.dom.minidom.parseString(region_text)
        	# Replace the view text with the output from toprettyxml() method
        	self.view.replace(edit, view_region, xml_dom.toprettyxml())
        except:
        	print("Not valid XML.")